import 'package:cloud_firestore/cloud_firestore.dart';

class Question {
   String text = "";
   String answer = "";
   String imageUrl = "";
  DocumentReference? reference;

  Question({text,answer,imageUrl,reference});

   factory Question.fromJson(Map<dynamic, dynamic> json) => Question(
       text: json['text'] as String,
       answer: json['answer'] as String,
       imageUrl: json['imageUrl'] as String?);

   Map<String, dynamic> toJson() => <String, dynamic>{
     'answer': answer,
     'text': text,
     'imageUrl': imageUrl,
   };

   factory Question.fromSnapshot(DocumentSnapshot snapshot) {
     final question = Question.fromJson(snapshot.data() as
     Map<String, dynamic>);
     question.reference = snapshot.reference;
     return question;
   }
}