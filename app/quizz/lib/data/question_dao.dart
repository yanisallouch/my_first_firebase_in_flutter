import 'package:cloud_firestore/cloud_firestore.dart';
import 'question.dart';
class QuestionDao {
// 1
  final CollectionReference collection =
  FirebaseFirestore.instance.collection('questions');

  void saveQuestion(Question question) {
    collection.add(question.toJson());
  }

  Stream<QuerySnapshot> getQuestionStream() {
    return collection.snapshots();
  }
}