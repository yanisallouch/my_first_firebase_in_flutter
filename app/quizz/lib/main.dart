import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quizz/data/data.dart';
import 'package:quizz/data/question_dao.dart';
import 'package:quizz/provider/homepage_provider.dart';
import 'package:quizz/views/homepage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        /*ChangeNotifierProvider<UserDao>(
          lazy: false,
          create: (_) => UserDao(),
        ),*/
        Provider<QuestionDao>(
          lazy: false,
          create: (_) => QuestionDao(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Questions/Réponses',
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
        home: ChangeNotifierProvider(
          create: (_) => HomepageProvider(0, getQuestions()),
          child: HomePage(title: "Questions/Réponses"),
        ),
      ),
    );
  }
}