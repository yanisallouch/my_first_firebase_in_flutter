import 'package:flutter/material.dart';
import 'package:quizz/data/question.dart';
import 'package:quizz/models/question_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class HomepageProvider with ChangeNotifier {
  List<QuestionModel> _questions = [];
  int _index;
  static CollectionReference questionRef = FirebaseFirestore.instance.collection('questions');

  HomepageProvider(this._index, this._questions);

  Future<void> getQuestionSync() async {

    QuerySnapshot query = await questionRef.get();
    List<QueryDocumentSnapshot> docs = query.docs;
    for (var doc in docs) {
      if (doc.data() != null) {
        var data = doc.data() as Map<String, dynamic>;
        _questions.add(QuestionModel.construct(question : data['text'], answer : data['answer'], imageUrl : data['imageUrl']));
      }
    }
  }

  void nextQuestion() {
    index = (index + 1) % questions.length;
    notifyListeners();
  }

  int get index => _index;

  set index(int value) {
    _index = value;
    notifyListeners();
  }

  List<QuestionModel> get questions => _questions;

  set questions(List<QuestionModel> value) {
    _questions = value;
    notifyListeners();
  }
}
